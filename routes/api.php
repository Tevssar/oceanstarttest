<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(
    ['prefix' => 'v01', 'middleware' => 'api'],
    function () {
        Route::group(
            ['prefix' => 'products'],
            function () {
                Route::get('/', [ProductsController::class, 'index']);
                Route::get('/{product}', [ProductsController::class, 'show']);
                Route::post('/', [ProductsController::class, 'store']);
                Route::put('/{id}', [ProductsController::class, 'update']);
                Route::delete('/{product}', [ProductsController::class, 'destroy']);
            }
        );

        Route::group(
            ['prefix' => 'category'],
            function () {
                Route::post('/', [CategoryController::class, 'store']);
                Route::delete('/{category}', [CategoryController::class, 'destroy']);
            }
        );
    }
);
