<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Model for ProductsCategories that connected Products and Categories
 *
 * @property array $fillable
 */
class ProductsCategories extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['products_id', 'category_id'];

}
