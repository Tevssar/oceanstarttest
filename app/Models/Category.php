<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Model for Product Category
 *
 * @property array $fillable
 * @method   products()
 * @method   firstProduct()
 */
class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name'];

     /**
      * Connects Category to Products by using ProductsCategories table.
      *
      * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
      */
    public function products()
    {
        return $this->belongsToMany(Products::class, ProductsCategories::class);
    }

     /**
      * Get one Product by using $this->products() relation.
      *
      * @return Product
      */
    public function firstProduct()
    {
        return $this->products()->take(1);
    }
}
