<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Model for Products
 *
 * @property array $fillable
 * @method   categories()
 */
class Products extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'price', 'currency', 'published'];

     /**
      * Connects Products to Category by using ProductsCategories table.
      *
      * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
      */
    public function categories()
    {
        return $this->belongsToMany(Category::class, ProductsCategories::class);
    }
}
