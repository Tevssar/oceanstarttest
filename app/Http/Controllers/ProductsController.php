<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterRequest;
use App\Http\Requests\ProductFormRequest;
use App\Models\Products;
use App\Models\ProductsCategories;
use Illuminate\Http\Request;

/**
 * Class for getting,creating, updating and deleting Products
 *
 * @method index(FilterRequest $request)
 * @method store(ProductFormRequest $request)
 * @method update(ProductFormRequest $request, Products $product)
 * @method show(Products $products)
 * @method destroy($id)
 */
class ProductsController extends Controller
{
    /**
     * Display filtered listing of Products.
     * request example "/api/v01/products?filters[products][name]=a&filters[categories][id]=5&filters[products][price][min]=2&filters[products][price][max]=3"
     *
     * @return \Illuminate\Http\return response()->json($data, 200, $headers);
     */
    public function index(FilterRequest $request)
    {
        $filters = $request->input('filters');
        $product = Products::with('categories');

        if (isset($filters['products'])) {
            foreach ((array)$filters['products'] as $field => $value) {
                if (is_array($value)) {
                    response()->json($field);
                    $product->whereBetween($field, [$value['min'], $value['max']]);
                } else {
                    $product = $product->where($field, 'LIKE', "%$value%");
                }
            }
        }

        if (isset($filters['categories'])) {
            foreach ((array)$filters['categories'] as $field => $value) {
                $product = $product-> whereHas(
                    'categories', function ($query) use ($field, $value) {
                        $query->where($field, 'LIKE', "%$value%");
                    }
                );
            }
        }

        return response()->json($product->get());
    }


    /**
     * Store a newly created Products and its Ccategories.
     *
     * @param  App\Http\Requests\ProductFormRequest $request
     *
     * @return \Illuminate\Http\return response()->json($data, 200, $headers);
     */
    public function store(ProductFormRequest $request)
    {
        $product = Products::create($request->all());
        foreach ($request->input('categories') as $category_id) {
            ProductsCategories::create(['products_id' => $product->id, 'category_id' => $category_id]);
        }
        return  response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products $products
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        $product = $products->first();
        return response()->json($product);
    }

    /**
     * Update the specified Product.
     *
     * @param  App\Http\Requests\ProductFormRequest $request
     * @param  \App\Models\Products $products
     *
     * @return \Illuminate\Http\return response()->json($data, 200, $headers);
     */
    public function update(ProductFormRequest $request, Products $product)
    {
        $product->fill($request->except(['id']));
        $product->save();
        return response()->json($product);
    }

    /**
     * Remove the specified Product.
     *
     * @param  \App\Models\Products $products
     *
     * @return \Illuminate\Http\return response()->json($data, 204, $headers);
     */
    public function destroy(Products $product)
    {
        if ($product->delete()) {
            return response()->json([], 204);
        }
    }
}
