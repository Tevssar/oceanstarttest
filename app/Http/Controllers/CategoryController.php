<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryFormRequest;
use App\Models\Category;
use Illuminate\Http\Request;

/**
 * Class for creating and deleting Categories
 *
 * @method store(CategoryFormRequest $request)
 * @method destroy($id)
 */
class CategoryController extends Controller
{

    /**
     * Store a newly created Category.
     *
     * @param App\Http\Requests\CategoryFormRequest $request
     *
     * @return \Illuminate\Http\return response()->json($data, 200, $headers);
     */
    public function store(CategoryFormRequest $request)
    {
        $product = Category::create($request->all());
        return  response()->json($product);
    }

    /**
     * Remove the specified Category.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\return response()->json($data, 200, $headers);
     */
    public function destroy($id)
    {
        $category = Category::with('firstProduct')->findOrFail($id);
        if (count($category->firstProduct) > 0) {
            $error = [
                'message' => 'this category have at least one product'
            ];
            return response()->json($error, 422);
        }

        if ($category->delete()) {
            return response()->json([], 204);
        }
    }
}
