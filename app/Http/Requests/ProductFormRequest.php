<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules that apply to the Product.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                "min:1",
                "max:255",
                 Rule::unique('products')->ignore($this->get('id'))
            ],
            'price' => [
                'required',
                'integer'
            ],
            'currency' => [
                'integer'
            ],
            'published' => [
                'integer'
            ],
            "categories" => [
                "required",
                "array",
                "min:2",
                "max:10"
            ],
            'categories.*' => [
                'integer'
            ],
        ];

    }
}
