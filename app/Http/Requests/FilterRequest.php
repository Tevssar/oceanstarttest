<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules that apply to the Product Filtering
     * @return array
     */
    public function rules()
    {
        return [
            'filters.products.name' => [
                'string',
                "min:1",
                "max:255",
            ],
            'filters.products.price.*' => [
                'integer',
            ],
            'filters.products.published' => [
                'integer',
            ],
            'filters.products.deleted' => [
                'integer',
            ],
            'filters.categories.id' => [
                'integer',
            ],
            'filters.categories.name' => [
                'string',
                "min:4",
                "max:255",
            ],
        ];
    }
}
